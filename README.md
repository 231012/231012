# 231012

# Light
 1. Развернуть три виртуальные машины с linux: web01, monl01, db01. Можно использовать любые хостинги и способы создания, например в Яндекс Облаке или локально на Vagrant.
 2. Установить сервер баз данных MySQL на виртуальную машину "db01"
 3. Установите и настройте Nginx на виртуальную машину "web01"
 4. Установите приложение phpmyadmin на виртуальной машине "web01" и настройте его для работы с вашим MySQL
 5. На виртуальной машиине "mon01" установите Grafana и Prometheus 
 6. Установить на "web01" и "db01" Prometheus-exporter и настроить сбор метрик для мониторинга на "mon01"
 7. Установите на "web01" приложение по инструкции https://piwigo.org/guides/install/manual

# Normal
 1. Разработайте Ansible-роли для установки и настройки nginx и phpmyadmin
 2. Разработайте Ansible-роль  для установки и настройки mysql. Продумайте функционад определения пути директории для файлов БД
 3. Напишите Ansible-playbook для развертывания вашего контура с приложением, задачи должны быть вынесены в атомарные роли разработанные ранее, конфигурация контура должна быть сделаан через ansible-inventory с переопределением стандартных переменных в group-vars
 4. Создайте бэкап-сценарий для MySQL базы данных, разверните сервер бэкапов и учтите сценарий настройки в ansible-роли для MySQL
 5. Настройте мониторинг nginx и MySQL в Prometheus, создайте панели мониторинга в Grafana, отображающие основные метрики (например, запросы в секунду, использование ЦПУ и памяти) nginx и MySQL
6. Настройте алертинг в Grafana, чтобы получать уведомления в случае проблем с приложением и серверами

# Hard
 1. Напишите Ansible-playbook для развертывания системы мониторинга. Вынесите в отдельные Ansible-роли для развертывания Grafana, Prometheus и экспортера
 2. Разверните  второй экземпляр MySQL в режиме мастер-мастер или мастер-слейв для обеспечения отказоустойчивости и резервного копирования данных. Учтите этот сценарий ancible-роли
 3. Разверните второй контур используя разработанные механизмы и включите его в монитроинг на сервере "mon01". Настройте сбор дополнительных метрик, таких как доли 1/0, потребление дискового пространства и дополнительные метрики nginx
 4. Покрыть Ansible-роли тестами в Molecule. Разработайте Gitlab CI пайплайн для автоматического тестирования ansible-ролей и внедрить его в репозитории с ролями
 5. Разработайте Gitlab CI пайплайн для автоматического развертывания системы мониторинга

# Expert
 1. Создайте документацию с описанием архитектуры, настроек и процедур обслуживания для вашей инфраструктуры
 2. Разместите приложение у вас в репозитории как рабочий проект, и продумайте флоу доставки приложения на контура. Разработать Gitlab CI пайплайн для автоматического развертывания окружений с приложением на контурах dev, test и prod
 3. Оформите развертывание всей инфраструктуры в Яндекс Облаке через Terraform. Провижн виртуальных машин оставить силами Ansible

# Краткое описание настроек и установки

### Равернем 4  виртуальных машины с помощью Vagrant

  "web01" c ip: "192.168.56.15", "db01" c ip: "192.168.56.16",  "mon01" c ip: "192.168.56.17" "backup" c ip: "192.168.56.18"

### Установка mysql-server на машине db01

Для этого используем команды:

    sudo apt-get update

Устанавливаем сам дистрибутив

    sudo apt install -y mysql-server
Проверяем результат

     systemctl status mysql

Мы должны увидеть нечто подобное

![Alt text](<Снимок экрана от 2023-11-25 16-05-05.png>)

Далее выполним настройку mysql-server

     sudo mysql

     sudo mysqladmin create myadmin1

     sudo mysql

     CREATE USER 
     
      myadmin1 identified by 'myadmin123';

    GRANT ALL
    on myadmin1.* to 'myadmin1';
    exit

    mysql -u myadmin1 -p myadmin1
    exit


### Далее на машине web01 установим NGINX

Для этого используем команды:

    sudo apt-get update

    sudo apt install nginx
После установки nginx подключаемся по ssh к машине web01. В файлах sites-awailable nginx.conf  необходимо указать соотвествующие настройки. Далее можно проверить конфигурацию командой 

    sudo nginx -t

Если не возникает никах ошибок, то необходимо перезапустить сервер коамндой 

    sudo systemctl restart nginx


### На машине web01 установим PHP-myadmin
Как и ранее сначала выполним команду 
    
    sudo apt-get update
 Далее установим сам  php-myadmin и соответстующий дистрибутив

    sudo apt install phpmyadmin php-mbstring php-zip php-gd php-json php-curl 

Когда установка будет завершена проверяем работоспособность php-myadmin перейдя в браузер. В нашем случае это 

    http://192.168.56.15/php-myadmin

Далее в веб интерфейсе исопльзуя логин и пароль заходим и проверяем работоспособность дистрибутива.
 
 В случае необходимости внесения изменений в конфигурацию можно использовать команду

    sudo dpkg-reconfigure phpmyadmin

### Установка Prometheus и Grafana на виртуальной машине mon01

Как и  ранее используем
    
     sudo apt-get update

Далее подготавливаем пользователя для установки
    
    sudo groupadd --system prometheus
и

    sudo useradd -s /sbin/nologin --system -g prometheus prometheus

Создаем директории для prometheus

    sudo mkdir /etc/prometheus
    sudo mkdir /etc/prometheus

Загружаем prometheus

    wget https://github.com/prometheus/prometheus/releases/download/v2.43.0/prometheus-2.45.1.linux-amd64.tar.gz

После чего извлекаем файлы из архива

    tar -xvf prometheus-2.45.1.linux-amd64.tar.gz

Далее переместим файлы prometheus, promtool в /usr/local/bin/

    sudo mv prometheus promtool /usr/local/bin/

Далее перемещаем consoles

    sudo mv consoles/ console_libraries/ /etc/prometheus/

И наконец перемещаем файл  prometheus.yml

    sudo mv prometheus.yml /etc/prometheus/prometheus.yml


После этого проверяем установленную версию

    prometheus --version

Затем создаем и добавлеям пользовтеля в группе prometheus

    sudo groupadd --system prometheus

    sudo useradd -s /sbin/nologin --system -g prometheus prometheus
Изменяем права
    
    sudo chown -R prometheus:prometheus /etc/prometheus/ /var/lib/prometheus/

    sudo chmod -R 775 /etc/prometheus/ /var/lib/prometheus/

В файле prometheus.service расположенном в директории /etc/systemd/system вносим ноебходимую конфигурацию чтобы наш мониторинг работал. 

После этого запускаем сервис prometheus и проверяем его статус

    sudo systemctl start prometheus

    sudo systemctl enable prometheus

    sudo systemctl status prometheus

Помним о необходимости открыть порт 9090 для доступа к веб интерфейсу prometheus

    sudo ufw status
    sudo ufw allow 9090
Для доступа используем адрес 
    
    curl http://your_server_ip:9090

### Установка grafana

 Скачиваем, устанавливаем и запускаем Grafana 

    wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -

    sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"

    sudo apt-get update

    sudo apt install grafana

    sudo systemctl status grafana-server

    sudo systemctl enable grafana-server

Доступ к веб интерфейсу Grafana  можно получить обратившись на 3000 порт 
    curl http://your-server-ip:3000

Перед этим не забываем открыть доступ к порту

        sudo ufw status

        sudo ufw allow 3000

Параметры доступа можно задать используя следующие команды

    sudo systemctl stop grafana-server

    sudo grafana-cli admin reset-admin-password <new_password>
    
    sudo systemctl start grafana-server

### Установка node exporter и mysql exporter

На машине web01 загрузим и установим node exporter используя следующие команды

    wget https://github.com/prometheus/node_exporter/releases/download/v1.7.0/node_exporter-1.7.0.linux-amd64.tar.gz
    
    tar xvfz node_exporter-1.7.0.linux-amd64.tar.gz

    cd node_exporter-1.7.0.linux-amd64


    ./node_exporter

Метрики node exporter доступны по команде

    curl http://your-server-ip:9100/metrics

Перед этим не забываем открыть доступ к порту

        sudo ufw status

        sudo ufw allow 9100
На машине db01 загрузим и установим mysql exporter используя следующие команды

    wget https://github.com/prometheus/mysqld_exporter/releases/download/v0.12.1/mysqld_exporter-0.12.1.linux-amd64.tar.gz

    tar xvfz mysqld_exporter-*.*-amd64.tar.gz
    cd mysqld_exporter-*.*-amd64

Создадим далее пользователя и предоставим ему необходиме права

    CREATE USER 'exporter'@'localhost' IDENTIFIED BY 'enter_password_here' WITH MAX_USER_CONNECTIONS 3;

    GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'exporter'@'localhost';

    export DATA_SOURCE_NAME='exporter:enter_password_here@(mysql_hostname:3306)/'


    ./mysqld_exporter
Метрики mysql exporter доступны по команде

    curl http://your-server-ip:9104/metrics

Перед этим не забываем открыть доступ к порту

        sudo ufw status

        sudo ufw allow 9104

После установки экспортеров необходимо внести изменения  параметры в файл prometheus.yml и перезапускаем prometheus

    ./prometheus --config.file=./prometheus.yml

Послен этого выполняем конфигурацию Grafana

# Установка приложения

На машине web01 скачиваем, настраиваем и устанавливаем приложение согласно инструкций на сайте. 

    https://ru.piwigo.org/guides/install/manual

     sudo apt install php-mbstring -y
     sudo apt install phpmyadmin -y 
     g up php7.4-opcache (7.4.3-4ubuntu2.19).
    
# В директрориях nginx и phpmyadmin прописаны роли для установки соотвествующего дистрибутива
Плейбук nginxplaybook.yml используюется для ролей nginx и phpmyadmin

# В директориях prometheus, grafana и exporter прописаны роли для установки соотвествующего дистрибутива
Плейбук рlaybook.yml используюется для ролей prometheus, grafana и exporter


# Настройка алертинга

Скачиваем, устанавливаем и запускаем alertmanager 

    wget https://github.com/prometheus/alertmanager/releases/download/v0.21.0/alertmanager-0.26.0.linux-amd64.tar.gz \
    && mkdir /etc/alertmanager /var/lib/prometheus/alertmanager \
    && tar zxvf alertmanager-*.linux-amd64.tar.gz \
    && cd alertmanager-*.linux-amd64 \
    && cp alertmanager amtool /usr/local/bin/ \
    && cp alertmanager.yml /etc/alertmanager

Назначаем пользователя и выдаём права

    useradd --no-create-home --shell /bin/false alertmanager \
    && chown -R alertmanager:alertmanager /etc/alertmanager /var/lib/prometheus/alertmanager \
    && chown alertmanager:alertmanager /usr/local/bin/{alertmanager,amtool}

После добавления автозапуска в файле alertmanager.service расположенный в директории /etc/systemd/system/ необходимо выполнить запуск

    systemctl daemon-reload \
    && systemctl enable alertmanager \
    && systemctl start alertmanager \
    && systemctl status alertmanager   

Далее проверяем работоспособность не веб интерфейсе

    curl http://your-server-ip:9093

И далее настраиваем оповещения.